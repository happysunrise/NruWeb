package com.happysunrise.nru.models;

/**
 * Created by liukaixin on 16/9/21.
 */
public class CompetitionLike {

    private Integer competitionId;

    private Integer like;

    public CompetitionLike() {
    }

    public Integer getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(Integer competitionId) {
        this.competitionId = competitionId;
    }

    public Integer getLike() {
        return like;
    }

    public void setLike(Integer like) {
        this.like = like;
    }
}
