package com.happysunrise.nru.models;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 团队.
 *
 * Created by liukaixin on 16/9/6.
 */
public class Team {

    /**
     *  团队ID
     */
    private Integer id;
    /**
     *  团队名称
     */
    private String name;

    /**
     *  团队照片
     */
    private String pic;

    /**
     * 竞赛id
     */
    private Integer competitionId;

    /**
     * 比赛名称
     */
    private String competitionName;

    /**
     * 团队创始人id
     */
    private Integer userId;

    /**
     * 创始人名字
     */
    private String userNickName;

    /**
     * 团队所属地区
     */
    private String areaIds;

    /**
     * 团队创始人学校名称
     */
    private String school;

    /**
     * 团队成员
     */
    private String memberIds;

    /**
     * 团队简介
     */
    private String intro;

    /**
     * 预计归档时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String predictAchieveTime;

    /**
     * 团队状态
     */
    private String status;

    /**
     * 团队创建时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String createTime;

    /**
     * 团队更新时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String updateTime;

    /**
     * 团队座右铭
     */
    private String motto;

    /**
     * 团队需求
     */
    private String require;

    /**
     * 团队人数
     */
    private Integer number;

    /**
     * 创始人简介.
     */
    private String creatorIntro;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCompetitionName() {
        return competitionName;
    }

    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getAreaIds() {
        return areaIds;
    }

    public void setAreaIds(String areaIds) {
        this.areaIds = areaIds;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getMemberIds() {
        return memberIds;
    }

    public void setMemberIds(String memberIds) {
        this.memberIds = memberIds;
    }

    public String getPredictAchieveTime() {
        return predictAchieveTime;
    }

    public void setPredictAchieveTime(String predictAchieveTime) {
        this.predictAchieveTime = predictAchieveTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getMotto() {
        return motto;
    }

    public void setMotto(String motto) {
        this.motto = motto;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getIntro(){
        return intro;
    }

    public void setIntro(String intro){
        this.intro = intro;
    }

    public String  getPic(){
        return pic;
    }

    public void setPic(String pic){
        this.pic = pic;
    }

    public Integer getCompetitionId() {
        return competitionId;
    }

    public void setCompetitionId(Integer competitionId) {
        this.competitionId = competitionId;
    }

    public String getRequire() {
        return require;
    }

    public void setRequire(String require) {
        this.require = require;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getCreatorIntro() {
        return creatorIntro;
    }

    public void setCreatorIntro(String creatorIntro) {
        this.creatorIntro = creatorIntro;
    }

    @Override
    public String toString() {
        return "Team{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pic='" + pic + '\'' +
                ", competitionId=" + competitionId +
                ", competitionName='" + competitionName + '\'' +
                ", userId=" + userId +
                ", userNickName='" + userNickName + '\'' +
                ", areaIds='" + areaIds + '\'' +
                ", school='" + school + '\'' +
                ", memberIds='" + memberIds + '\'' +
                ", intro='" + intro + '\'' +
                ", predictAchieveTime='" + predictAchieveTime + '\'' +
                ", status='" + status + '\'' +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", motto='" + motto + '\'' +
                ", require='" + require + '\'' +
                ", number=" + number +
                ", creatorIntro='" + creatorIntro + '\'' +
                '}';
    }
}

