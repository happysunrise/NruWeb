package com.happysunrise.nru.models;

import com.happysunrise.nru.bases.BaseBean;

/**
 * Created by Jacob on 2016/10/5.
 */
public class TeamMember extends BaseBean {

    private Integer teamId;

    private Integer userId;

    private String username;

    private String nickName;

    private String userPic;

    private String expert;

    public TeamMember() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getUserPic() {
        return userPic;
    }

    public void setUserPic(String userPic) {
        this.userPic = userPic;
    }

    public String getExpert() {
        return expert;
    }

    public void setExpert(String expert) {
        this.expert = expert;
    }
}
