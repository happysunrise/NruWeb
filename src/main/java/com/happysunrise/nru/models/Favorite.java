package com.happysunrise.nru.models;

import com.alibaba.fastjson.annotation.JSONField;
import com.happysunrise.nru.bases.BaseBean;

import java.util.Date;

/**
 * Created by Jacob on 2016/9/17.
 */
public class Favorite extends BaseBean {
    /**
     *  收藏名.
     */
    private String starName;

    /**
     *  收藏Id.
     */
    private Integer starId;

    /**
     * 收藏者id.
     */
    private Integer userId;

    /**
     *  类别,1:竞赛, 2:团队.
     */
    private Integer category;

    /**
     * 创建日期.
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String createTime;

    /**
     * 更新日期.
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    private String updateTime;

    public Favorite() {

    }

    public String getStarName() {
        return starName;
    }

    public void setStarName(String starName) {
        this.starName = starName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getStarId() {
        return starId;
    }

    public void setStarId(Integer starId) {
        this.starId = starId;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "Favorite{" +
                "starName='" + starName + '\'' +
                ", starId=" + starId +
                ", userId=" + userId +
                ", category=" + category +
                ", createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                '}';
    }
}
