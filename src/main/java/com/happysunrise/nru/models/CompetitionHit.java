package com.happysunrise.nru.models;

/**
 * Created by liukaixin on 16/9/21.
 */
public class CompetitionHit {

    private Integer CompetitionId;

    private Integer hit;

    public CompetitionHit() {

    }

    public Integer getCompetitionId() {
        return CompetitionId;
    }

    public void setCompetitionId(Integer competitionId) {
        CompetitionId = competitionId;
    }

    public Integer getHit() {
        return hit;
    }

    public void setHit(Integer hit) {
        this.hit = hit;
    }
}
