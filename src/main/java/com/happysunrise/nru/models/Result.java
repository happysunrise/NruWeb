package com.happysunrise.nru.models;

/**
 *
 * 结果集.
 *
 * Created by liukaixin on 16/7/16.
 * @param <T>
 */
public class Result<T> {

    public static final Integer FAILED = -1;

    public static final Integer SUCCESS = 1;

    private Integer code;

    private String message;

    private T bean;

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public T getBean() {
        return bean;
    }

    public void setBean(T bean) {
        this.bean = bean;
    }
}
