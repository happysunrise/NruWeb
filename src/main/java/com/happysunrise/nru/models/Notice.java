package com.happysunrise.nru.models;

import com.happysunrise.nru.bases.BaseBean;

/**
 * Created by liukaixin on 16/9/22.
 */
public class Notice extends BaseBean{

    private Integer idForCategory;

    private String title;

    private String info;

    private Integer category;

    private String createTime;

    private Integer userId;

    private Integer read;

    public Integer getIdForCategory() {
        return idForCategory;
    }

    public void setIdForCategory(Integer idForCategory) {
        this.idForCategory = idForCategory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Integer getCategory() {
        return category;
    }

    public void setCategory(Integer category) {
        this.category = category;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRead() {
        return read;
    }

    public void setRead(Integer read) {
        this.read = read;
    }
}
