package com.happysunrise.nru.models;

import com.happysunrise.nru.bases.BaseBean;

/**
 * Created by liukaixin on 16/9/19.
 */
public class Apply extends BaseBean {

    /**
     * 能力.
     */
    private String ability;

    /**
     * 申请人id.
     */
    private Integer applyUserId;

    /**
     * 团队id.
     */
    private Integer teamId;

    /**
     * 团队名称.
     */
    private String teamName;

    /**
     * 团队创始人id.
     */
    private Integer teamCreatorId;

    private String competitionName;

    /**
     * 创始时间.
     */
    private String createTime;

    /**
     * 审核状态. 1: 未审核 2:已同意 3:已拒绝
     */
    private Integer status;

    // 数据库中没有但前端需要端字段
    private String applyUserRealName;

    private String applyUserDegree;

    private String applyUserSchool;

    private String applyUserPic;

    private String applyUserNickName;

    private String applyUserContact;

    public Apply() {
        // mybatis need an empty constructor
    }

    public String getAbility() {
        return ability;
    }

    public void setAbility(String ability) {
        this.ability = ability;
    }

    public Integer getApplyUserId() {
        return applyUserId;
    }

    public void setApplyUserId(Integer applyUserId) {
        this.applyUserId = applyUserId;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getTeamCreatorId() {
        return teamCreatorId;
    }

    public void setTeamCreatorId(Integer teamCreatorId) {
        this.teamCreatorId = teamCreatorId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getApplyUserRealName() {
        return applyUserRealName;
    }

    public void setApplyUserRealName(String applyUserRealName) {
        this.applyUserRealName = applyUserRealName;
    }

    public String getApplyUserDegree() {
        return applyUserDegree;
    }

    public void setApplyUserDegree(String applyUserDegree) {
        this.applyUserDegree = applyUserDegree;
    }

    public String getApplyUserSchool() {
        return applyUserSchool;
    }

    public void setApplyUserSchool(String applyUserSchool) {
        this.applyUserSchool = applyUserSchool;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getCompetitionName() {
        return competitionName;
    }

    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    public String getApplyUserPic() {
        return applyUserPic;
    }

    public void setApplyUserPic(String applyUserPic) {
        this.applyUserPic = applyUserPic;
    }

    public String getApplyUserNickName() {
        return applyUserNickName;
    }

    public void setApplyUserNickName(String applyUserNickName) {
        this.applyUserNickName = applyUserNickName;
    }

    public String getApplyUserContact() {
        return applyUserContact;
    }

    public void setApplyUserContact(String applyUserContact) {
        this.applyUserContact = applyUserContact;
    }
}
