package com.happysunrise.nru.models;

import com.happysunrise.nru.bases.BaseBean;

/**
 * 用户推送实体.
 *
 * Created by liukaixin on 16/10/7.
 */
public class UserPush extends BaseBean{

    private Integer userId;

    private String clientId;

    public UserPush() {
        // mybatis need an empty constructor
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "UserPush{" +
                "userId=" + userId +
                ", clientId='" + clientId + '\'' +
                '}';
    }
}
