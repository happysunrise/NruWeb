package com.happysunrise.nru.models;

import com.happysunrise.nru.bases.BaseBean;

/**
 * Created by liukaixin on 16/9/18.
 */
public class UserTeam extends BaseBean {

    private Integer userId;

    private Integer teamId;

    private Integer role;

    private String teamName;

    private Integer creatorId;

    private String competitionName;

    private String createTime;

    public UserTeam() {
        // mybatis need an empty constructor
    }

    public String getCompetitionName() {
        return competitionName;
    }

    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getTeamId() {
        return teamId;
    }

    public void setTeamId(Integer teamId) {
        this.teamId = teamId;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public Integer getCreatorId() {
        return creatorId;
    }

    public void setCreatorId(Integer creatorId) {
        this.creatorId = creatorId;
    }

    @Override
    public String toString() {
        return "UserTeam{" +
                "userId=" + userId +
                ", teamId=" + teamId +
                ", role=" + role +
                ", teamName='" + teamName + '\'' +
                ", creatorId=" + creatorId +
                ", competitionName='" + competitionName + '\'' +
                ", createTime='" + createTime + '\'' +
                '}';
    }
}
