package com.happysunrise.nru.configs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by liukaixin on 16/9/30.
 */
@Configuration
public class WebMvcConfigurer extends WebMvcConfigurerAdapter {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(WebMvcConfigurer.class);

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 多个拦截器组成一个拦截器链
        // addPathPatterns 用于添加拦截规则
        // excludePathPatterns 用户排除拦截
        registry.addInterceptor(new HandlerInterceptor() {
            @Override
            public boolean preHandle(HttpServletRequest request,
                                     HttpServletResponse response,
                                     Object handler) throws Exception {
                LOGGER.debug(">>>MyInterceptor2>>>>>>>在请求处理之前进行调用（Controller方法调用之前）");
                return true;// 只有返回true才会继续向下执行，返回false取消当前请求
            }

            @Override
            public void postHandle(HttpServletRequest request,
                                   HttpServletResponse response,
                                   Object handler,
                                   ModelAndView modelAndView) throws Exception {

            }

            @Override
            public void afterCompletion(HttpServletRequest request,
                                        HttpServletResponse response,
                                        Object handler,
                                        Exception ex) throws Exception {

            }
        }).addPathPatterns("/racing-union/.*");
        super.addInterceptors(registry);
    }
}