package com.happysunrise.nru.controllers;

import com.happysunrise.nru.models.*;
import com.happysunrise.nru.services.IUserService;
import com.happysunrise.nru.services.imp.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * 用户接口.
 *
 * Created by liukaixin on 16/9/4.
 */
@RestController
@RequestMapping("/racing-union/user")
public class UserController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(UserController.class);

    @Autowired
    private IUserService userService;

    /**
     * 登录.
     *
     * @param user 用户
     * @return 用户
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result<User>>
    login(@RequestBody User user) {
        LOGGER.info("receive request>>login:[{}], [{}] ", user.getUsername(), user.getPassword());
        Result<User> result = userService.login(user);
        return ResponseEntity.ok(result);
    }

    /**
     * 注册.
     *
     * @param user 用户
     * @return 第几位用户
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result<User>>
    register(@RequestBody User user) {
        LOGGER.info("receive request>>register: user = [{}] ", user.toString());
        Result<User> result = userService.register(user);
        return ResponseEntity.ok(result);
    }

    /**
     * 更改用户.
     *
     * @param user 用户
     * @return 用户
     */
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result<String>>
    update(@RequestBody User user) {
        LOGGER.info("receive request>>update:[{}] ", user);
        Result<String> result = userService.update(user);
        return ResponseEntity.ok(result);
    }

    /**
     * 添加我的竞赛
     *
     * @param competition   竞赛
     * @param userId        用户Id
     * @return  竞赛列表
     */
    @RequestMapping(value = "/addMyCompetition/{userId}",
            method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result<String>>
    addMyCompetition(@RequestBody Competition competition,
                     @PathVariable("userId") Integer userId) {
        LOGGER.info("receive request>>addMyCompetition:" +
                "userId = [{}]" +
                "competition [{}]", userId, competition.toString());
        Result<String> result = userService.addMyCompetition(
                competition, userId);
        return ResponseEntity.ok(result);
    }

    /**
     * 添加我的团队
     *
     * @param team  我的团队
     * @return  团队列表
     */
    @RequestMapping(value = "/addMyTeam",
            method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result<String>>
    addMyTeam(@RequestBody UserTeam team) {
        LOGGER.info("receive request>>addMyTeam:" +
                "team [{}]", team.toString());
        Result<String> result = userService.addMyTeam(
                team);
        return ResponseEntity.ok(result);
    }

    /**
     * 获取我的团队
     *
     * @param page  页码
     * @param rows  条数
     * @param userId    用户Id
     * @return
     */
    @RequestMapping(value = "/getTeamsByUserId/{page}/{rows}/{userId}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<List<UserTeam>>>
    getTeamsByUserId(@PathVariable Integer page,
                     @PathVariable Integer rows,
                     @PathVariable Integer userId) {
        LOGGER.info("receive request>>get teams by user Id " +
                ":page = [{}], rows = [{}], userId = [{}] ", page, rows, userId);
        Result<List<UserTeam>> result = userService.getTeamsByUserId(
                page, rows, userId);
        return ResponseEntity.ok(result);
    }

    /**
     * 添加我的收藏.
     *
     * @param favorite 我的收藏
     * @return .
     */
    @RequestMapping(value = "/favorite",
            method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result<String>>
    favorite(@RequestBody Favorite favorite) {
        LOGGER.info("receive request>>favorite:" +
                "favorite = [{}]" , favorite.toString());
        Result<String> result = userService.favorite(favorite);
        return ResponseEntity.ok(result);
    }

    /**
     * 获取我的收藏
     *
     * @param page  页码
     * @param rows  条数
     * @param userId 用户Id
     * @return  我的收藏
     */
    @RequestMapping(value = "/getFavoritesByUserId/{page}/{rows}/{userId}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<List<Favorite>>>
    getFavoritesByUserId(@PathVariable("page") Integer page,
                         @PathVariable("rows") Integer rows,
                         @PathVariable("userId") Integer userId) {
        LOGGER.info("receive request>>get favorites by user Id " +
                ":page = [{}], rows = [{}], userId = [{}] ", page, rows, userId);
        Result<List<Favorite>> result = userService.getFavoritesByUserId(
                page, rows, userId);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/getUserInfoById/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<User>>
    getUserInfoById(@PathVariable("userId") Integer userId) {
        LOGGER.info("receive request>>getUserInfoById, " +
                "userId = [{}] ", userId);
        Result<User> result = userService.getUserInfoById(userId);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/getFavoriteOrNot/{starId}/{userId}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<Boolean>>
    getFavoriteOrNot(@PathVariable("starId") Integer starId,
                         @PathVariable("userId") Integer userId) {
        LOGGER.info("receive request>>getFavoriteOrNot " +
                ":starId = [{}], userId = [{}] ", starId, userId);
        Result<Boolean> result = userService.getFavoriteOrNot(
                starId, userId);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/cancelFavorite/{starId}/{userId}",
            method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity<Result<String>>
    cancelFavorite(@PathVariable("starId") Integer starId,
                     @PathVariable("userId") Integer userId) {
        LOGGER.info("receive request>>cancelFavorite by star id and user Id " +
                ":starId = [{}], userId = [{}] ", starId, userId);
        Result<String> result = userService.cancelFavorite(
                starId, userId);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/push/addUserPush",
            method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result<Integer>>
    addUserPush(@RequestBody UserPush userPush) {
        LOGGER.info("receive request>>addUserPush userPush = %s ", userPush.toString());
        Result<Integer> result = userService.addUserPush(
                userPush);
        return ResponseEntity.ok(result);
    }
}
