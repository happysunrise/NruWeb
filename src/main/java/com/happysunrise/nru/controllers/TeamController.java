package com.happysunrise.nru.controllers;

import com.happysunrise.nru.models.*;
import com.happysunrise.nru.services.ITeamService;
import com.happysunrise.nru.services.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 团队接口.
 * <p>
 * Created by liukaixin on 16/7/16.
 */
@RestController
@RequestMapping("/racing-union/team")
public class TeamController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(TeamController.class);

    @Autowired
    private ITeamService teamService;
    @Autowired
    private IUserService userService;

    /**
     * 获得发布状态的团队.
     *
     * @param page 页码
     * @param rows 条数
     * @return .
     */
    @RequestMapping(value = "/getReleasedTeams/{page}/{rows}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<List<Team>>>
    getReleaseTeams(@PathVariable("page") Integer page, @PathVariable("rows") Integer rows) {
        LOGGER.info("receive request>>get released get released teams:page = [{}], rows = [{}] ",
                page, rows);
        Result<List<Team>> result = teamService.getReleasedTeams(page, rows);
        return ResponseEntity.ok(result);
    }

    /**
     * 添加申请加入团队记录.
     *
     * @param apply 申请
     * @return .
     */
    @RequestMapping(value = "/apply/addApply", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result<String>>
    getReleaseTeams(@RequestBody Apply apply) {
        LOGGER.info("receive request>>addApply, apply = [{}] ",
                apply);
        Result<String> result = teamService.addJoinTeamApply(apply);
        return ResponseEntity.ok(result);
    }


    /**
     * 按团队Id查询团队.
     *
     * @param id 团队id
     * @return
     */
    @RequestMapping(value = "/getTeamDescById/{id}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<Team>> getTeamDescById(@PathVariable Integer id) {
        LOGGER.info("receive request>>getTeamDescById by id " +
                ":id = [{}]", id);
        Result<Team> result = teamService.getTeamDescById(id);
        return ResponseEntity.ok(result);
    }

    /**
     * 根据用户id获取团队成员详情.
     *
     * @param userId userId
     * @return .
     */
    @RequestMapping(value = "/getTeamMemberInfoByTeamIdAndUserId/{teamId}/{userId}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<User>>
    getTeamMemberInfoByTeamIdAndUserId(@PathVariable("teamId") Integer teamId,
                                       @PathVariable("userId") Integer userId) {
        LOGGER.info("receive request>>getTeamMemberInfoByTeamIdAndUserId, teamId = [{}], userId = [{}] ",
                teamId, userId);
        Result<User> result = userService.getUserByTeamIdAndUserId(teamId, userId);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/getTeamMembers/{teamId}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<List<TeamMember>>>
    getTeamMembers(@PathVariable("teamId") Integer teamId) {
        LOGGER.info("receive request>>getTeamMembers, teamId = [{}]",
                teamId);
        Result<List<TeamMember>> result = teamService.getTeamMembers(teamId);
        return ResponseEntity.ok(result);
    }

    /**
     * 创建团队.
     *
     * @param team 团队
     * @return .
     */
    @RequestMapping(value = "/createTeam", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result<String>>
    createTeam(@RequestBody Team team) {
        LOGGER.info("receive request>>createTeam, team = [{}] ",
                team.toString());
        Result<String> result = teamService.createTeam(team);
        return ResponseEntity.ok(result);
    }

    /**
     * 根据竞赛id获取团队列表.
     *
     * @param page          页码
     * @param rows          条数
     * @param competitionId 竞赛id
     * @return
     */
    @RequestMapping(value = "/getTeamsByCompetitionId/{page}/{rows}/{competitionId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<List<Team>>>
    getTeamsByCompetitionId(@PathVariable("page") Integer page,
                            @PathVariable("rows") Integer rows,
                            @PathVariable("competitionId") Integer competitionId) {
        LOGGER.info("receive request>>getTeamsByCompetitionId, competitionId = [{}] ",
                competitionId);
        Result<List<Team>> result = teamService.getTeamsByCompetitionId(page, rows, competitionId);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/apply/getApplyStatus/{teamId}/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<Integer>>
    getApplyStatus(@PathVariable("teamId") Integer teamId,
                  @PathVariable("userId") Integer userId) {
        LOGGER.info("receive request>>getApplyStatus, teamId = [{}], userId = [{}] ",
                teamId, userId);
        Result<Integer> result = teamService.getApplyStatus(teamId, userId);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/apply/getApplyById/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<Apply>>
    getApplyById(@PathVariable("id") Integer id) {
        LOGGER.info("receive request>>getApplyById, applyId = [{}] ",
                id);
        Result<Apply> result = teamService.getApplyById(id);
        return ResponseEntity.ok(result);
    }

    @RequestMapping(value = "/apply/updateApplyStatus/{applyId}/{status}",
            method = RequestMethod.PUT)
    @ResponseBody
    public ResponseEntity<Result<String>>
    updateApplyStatus(@PathVariable("applyId") Integer applyId,
                      @PathVariable("status") Integer status) {
        LOGGER.info("receive request>>updateApplyStatus, applyId = [{}]," +
                " status = [{}]",
                applyId, status);
        Result<String> result = teamService.updateApplyStatus(applyId, status);
        return ResponseEntity.ok(result);
    }

    /**
     * 获取申请人同比赛其他团队的申请状态.
     *
     * @param applyUserId     申请人id
     * @param competitionName 比赛名称
     * @return 申请状态
     */
    @RequestMapping(value = "/apply/getOtherApplyStatus/{applyUserId}/{competitionName}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<Integer>>
    getOtherApplyStatus(@PathVariable("applyUserId") Integer applyUserId,
                        @PathVariable("competitionName") String competitionName) {
        LOGGER.info("receive request>>getOtherApplyStatus, applyUserId = [{}], competitionName = [{}]",
                applyUserId, competitionName);
        Result<Integer> result = teamService.getOtherApplyStatus(applyUserId, competitionName);
        return ResponseEntity.ok(result);
    }

    /**
     * 获取我的申请列表.
     *
     * @param page   页码
     * @param rows   条数
     * @param userId 用户id
     * @return
     */
    @RequestMapping(value = "/apply/getMyAppliesByUserId/{page}/{rows}/{userId}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<List<Apply>>>
    getMyAppliesByUserId(@PathVariable("page") Integer page,
                         @PathVariable("rows") Integer rows,
                         @PathVariable("userId") Integer userId) {
        LOGGER.info("receive request>>getMyAppliesByUserId, page = [{}], rows = [{}], userId = [{}]",
                page, rows, userId);
        Result<List<Apply>> result = teamService.getMyAppliesByUserId(page, rows, userId);
        return ResponseEntity.ok(result);
    }
}
