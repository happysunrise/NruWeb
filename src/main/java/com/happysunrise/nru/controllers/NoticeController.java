package com.happysunrise.nru.controllers;

import com.happysunrise.nru.models.Notice;
import com.happysunrise.nru.models.Result;
import com.happysunrise.nru.services.INoticeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by liukaixin on 16/9/22.
 */
@RestController
@RequestMapping("/racing-union/notice")
public class NoticeController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(NoticeController.class);

    @Autowired
    private INoticeService noticeService;

    /**
     * 根据用户id和类别id获取通知.
     *
     * @param page   页数
     * @param rows   条数
     * @param userId 用户id
     * @return
     */
    @RequestMapping(value = "getNoticesByUserId/{page}/{rows}/{categoryId}/{userId}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<List<Notice>>>
    getNoticeByUserIdAndCategory(@PathVariable("page") Integer page, @PathVariable("rows") Integer rows,
                                 @PathVariable("userId") Integer userId, @PathVariable("categoryId") Integer category)
    {
        LOGGER.info("receive request>>getNoticeByUserIdAndCategory:page = [{}], rows = [{}]," +
                "userId = [{}], category = [{}]",
                page, rows, userId, category);
        Result<List<Notice>> result = noticeService.getNoticeByUserIdAndCategory(page, rows,
                userId, category);
        return ResponseEntity.ok(result);
    }

}
