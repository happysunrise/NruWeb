package com.happysunrise.nru.controllers;

import com.happysunrise.nru.models.Competition;
import com.happysunrise.nru.models.Result;
import com.happysunrise.nru.services.ICompetitionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

/**
 * CompetitionController.
 *
 * Created by liukaixin on 16/8/20.
 */
@RestController
@RequestMapping("/racing-union/competition")
public class CompetitionController {

    private static final Logger LOGGER =
            LoggerFactory.getLogger(CompetitionController.class);

    @Autowired
    private ICompetitionService competitionService;

    /**
     * 添加竞赛.
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<Result>
    addCompetition(@RequestBody Competition competition) {
        LOGGER.info("receive request>>add competition:[{}] ", competition);
        Result<Integer> result = competitionService.addCompetition(competition);
        return ResponseEntity.ok(result);
    }

    /**
     * 获取发布状态竞赛.
     */
    @RequestMapping(value = "/getReleasedCompetitions/{page}/{rows}",
    method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result>
    getReleasedCompetitions(@PathVariable Integer page,
                                                   @PathVariable Integer rows) {
        LOGGER.info("receive request>>get released competitions:page = [{}], rows = [{}] ",
                page, rows);
        Result<List<Competition>> result = competitionService.getReleasedCompetitionsBy(
                page, rows);
        return ResponseEntity.ok(result);
    }

    /**
     * 按类型获取发布状态竞赛.
     */
    @RequestMapping(value = "/getReleasedCompetitionsByType/{page}/{rows}/{type}",
    method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result>
    getReleasedCompetitionsByType(@PathVariable Integer page,
                                                               @PathVariable Integer rows,
                                                               @PathVariable String type) {
        LOGGER.info("receive request>>get released competitions by type " +
                ":page = [{}], rows = [{}], type = [{}] ", page, rows, type);
        Result<List<Competition>> result = competitionService.getReleasedCompetitionByType(
                page, rows, type);
        return ResponseEntity.ok(result);
    }

    /**
     * 按名称模糊查询发布状态竞赛列表
     */
    @RequestMapping(value = "/getReleasedCompetitionsByFuzzyName/{page}/{rows}/{name}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result>
    getReleasedCompetitionsByFuzzyName(@PathVariable Integer page,
                                       @PathVariable Integer rows,
                                       @PathVariable String name) {
        LOGGER.info("receive request>>get released competitions by name " +
                ":page = [{}], rows = [{}], name = [{}] ", page, rows, name);
        Result<Set<Competition>> result = competitionService.getReleasedCompetitionsByFuzzyName(
                page, rows, name);
        return ResponseEntity.ok(result);
    }

    /**
     * 按Id查询竞赛.
     */
    @RequestMapping(value = "getCompetitionDesc/{id}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<Competition>>
    getCompetitionDescById(@PathVariable Integer id) {
        LOGGER.info("receive request>>get competitions by id " +
                ":id = [{}]", id);
        Result<Competition> result = competitionService.getCompetitionDescById(
                id);
        return ResponseEntity.ok(result);
    }

    /**
     * 按名称精确查询竞赛.
     */
    @RequestMapping(value = "/getCompetitionDescByExactName/{name}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result>
    getCompetitionDescByExactName(@PathVariable String name) {
        LOGGER.info("receive request>>get competitions by id " +
                ":name = [{}]", name);
        Result<Competition> result = competitionService.getCompetitionDescByExactName(
                name);
        return ResponseEntity.ok(result);
    }

    /**
     * 更新竞赛.
     */
    @RequestMapping(value = "/updateCompetition", method = RequestMethod.POST)
    public ResponseEntity<Result>
    updateCompetition(@RequestBody Competition competition) {
        LOGGER.info("receive request>>update competition:[{}] ", competition);
        Result result = competitionService.updateCompetition(competition);
        return ResponseEntity.ok(result);
    }


    /**
     * 获取我的竞赛
     *
     * @param page      页码
     * @param rows      条数
     * @param userId    用户ID
     * @return          竞赛
     */
    @RequestMapping(value = "/getCompetitionsByUserId/{page}/{rows}/{userId}",
            method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Result<List<Competition>>>
    getCompetitionsByUserId(@PathVariable Integer page,
                            @PathVariable Integer rows,
                            @PathVariable Integer userId) {
        LOGGER.info("receive request>>get competitions by user Id " +
                ":page = [{}], rows = [{}], userId = [{}] ", page, rows, userId);
        Result<List<Competition>> result = competitionService.getCompetitionsByUserId(
                page, rows, userId);
        return ResponseEntity.ok(result);
    }
}
