package com.happysunrise.nru.managers;

import com.happysunrise.nru.models.Apply;
import com.happysunrise.nru.models.Notice;
import com.happysunrise.nru.models.Team;
import com.happysunrise.nru.models.TeamMember;

import java.util.List;
import java.util.Set;

/**
 * Created by liukaixin on 16/9/6.
 */
public interface ITeamManager {

    /**
     * 获取发布状态的团队列表.
     *
     * @param page 页码
     * @param rows 条数
     * @return 团队列表
     */
    List<Team> getReleasedTeams(Integer page, Integer rows);

    /**
     * 添加加入团队申请.
     *
     * @param apply 申请
     */
    void addJoinTeamApply(Apply apply);

    /**
     * 根据团队id获取团队详情.
     *
     * @param id 团队id
     * @return 团队实体
     */
    Team getTeamDescById(Integer id);

    /**
     * 根据团队id获取团队名称和竞赛名称
     *
     * @param id 团队id
     * @return
     */
    Team getTeamNameAndCompetitionNameById(Integer id);

    /**
     * 创建团队
     *
     * @param team 团队
     */
    void createTeam(Team team);

    /**
     * 根绝竞赛id获取团队列表.
     *
     * @param page          页码
     * @param rows          条数
     * @param competitionId 竞赛id
     * @return 团队列表
     */
    List<Team> getTeamsByCompetitionId(Integer page, Integer rows, Integer competitionId);

    /**
     * 根据团队id和申请人id判断是否已申请过加入团队.
     *
     * @param teamId 团队id
     * @param userId 申请人id
     * @return
     */
    Integer getApplyStatus(Integer teamId, Integer userId);

    /**
     * 根据申请id获取申请.
     *
     * @param id 申请id
     * @return
     */
    Apply getApplyById(Integer id);

    /**
     * 根据申请id更改状态.
     *
     * @param applyId 申请id
     * @param status  要更改的状态
     * @return
     */
    Integer updateApplyStatus(Integer applyId, Integer status);

    /**
     * 获取申请人同比赛其他团队的申请状态.
     *
     * @param applyUserId     申请人id
     * @param competitionName 比赛名称
     * @return 申请状态
     */
    Set<Integer> getOtherApplyStatus(Integer applyUserId, String competitionName);

    /**
     * 添加团队成员
     *
     * @param teamMember 团队成员.
     * @return .
     */
    Integer addTeamMember(TeamMember teamMember);

    /**
     * 根据团队名称查找团队Id.
     *
     * @param name 团队名称
     * @return
     */
    Integer getTeamIdByName(String name);

    /**
     * 获取我的申请列表.
     *
     * @param page   页码
     * @param rows   条数
     * @param userId 用户id
     * @return
     */
    List<Apply> getMyAppliesByUserId(Integer page, Integer rows, Integer userId);

    /**
     * 根据UserId和竞赛id获取团队id
     *
     * @param userId        创始人id
     * @param competitionId 竞赛id
     * @return
     */
    Integer getTeamIdByUserIdAndCompId(Integer userId, Integer competitionId);

    /**
     * 根据userId获取我要审核的团队申请.
     *
     * @param page   页码
     * @param rows   条数
     * @param userId 用户id（team_creator_id)
     * @return
     */
    List<Notice> getMyChecksByUserId(Integer page, Integer rows, Integer userId);

    List<TeamMember> getTeamMembers(Integer teamId);

    Integer updateTeamMemeberNum(Integer teamId);
}