package com.happysunrise.nru.managers.imp;

import com.github.pagehelper.PageHelper;
import com.happysunrise.nru.daos.ICompetitionDao;
import com.happysunrise.nru.daos.ICompetitionHitDao;
import com.happysunrise.nru.daos.ICompetitionLikeDao;
import com.happysunrise.nru.daos.IUserCompetitionDao;
import com.happysunrise.nru.enumerations.CompetitionStatusEnum;
import com.happysunrise.nru.enumerations.QueryOrderEnum;
import com.happysunrise.nru.managers.ICompetitionManager;
import com.happysunrise.nru.models.Competition;
import io.swagger.models.auth.In;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * CompetitionManager的实现.
 * <p>
 * Created by liukaixin on 16/7/16.
 */
@Component
public class CompetitionManager implements ICompetitionManager {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CompetitionManager.class);

    @Autowired
    ICompetitionDao competitionDao;
    @Autowired
    ICompetitionHitDao competitionHitDao;
    @Autowired
    ICompetitionLikeDao competitionLikeDao;
    @Autowired
    IUserCompetitionDao userCompetitionDao;

    /**
     * 添加竞赛.
     *
     * @param competition 竞赛实体
     * @return 插入结果
     * @throws RuntimeException
     */
    @Override
    public Integer addCompetition(Competition competition) {
        return competitionDao.addCompetition(competition);
    }

    /**
     * 获取发布的竞赛.
     *
     * @param page 页码
     * @param rows 每页显示条数
     * @return 竞赛列表
     */
    @Override
    public List<Competition> getReleasedCompetitionsBy(Integer page,
                                                       Integer rows) {
        PageHelper.startPage(page, rows);
        PageHelper.orderBy(QueryOrderEnum.COMPETITION_SIGN_END_TIME_DESC.getOrder());
        return competitionDao.selectCompetitionsByStatus(
                CompetitionStatusEnum.RELEASE.getStatus());
    }

    /**
     * 根据页码,每页显示条数,竞赛类型获得已发布的竞赛.
     *
     * @param page 页码
     * @param rows 每页显示的条数
     * @param type 类型
     * @return 竞赛列表
     */
    @Override
    public List<Competition> getReleasedCompetitionsByType(
            Integer page, Integer rows, String type) {
        PageHelper.startPage(page, rows);
        PageHelper.orderBy(QueryOrderEnum.COMPETITION_SIGN_END_TIME_DESC.getOrder());
        try {
            return competitionDao
                    .selectCompetitionsByConditions(
                            "%" + type + "%",
                            CompetitionStatusEnum.RELEASE.getStatus());
        } catch (RuntimeException e) {
            LOGGER.error("getReleasedCompetitionsByCondition:", e);
            return new ArrayList<>();
        }
    }

    /**
     * 根据名称模糊查询竞赛列表.
     *
     * @param page 页码
     * @param rows 条数
     * @param name 竞赛名称（模糊）
     * @return 竞赛列表
     */
    @Override
    public Set<Competition> getReleasedCompetitionsByFuzzyName(
            Integer page, Integer rows, String name) {
        PageHelper.startPage(page, rows);
        PageHelper.orderBy(QueryOrderEnum.COMPETITION_SIGN_END_TIME_DESC.getOrder());
        try {
            return competitionDao
                    .selectCompetitionsByFuzzyNameAndStatus(
                            "%" + name + "%",
                            CompetitionStatusEnum.RELEASE.getStatus());
        } catch (RuntimeException e) {
            LOGGER.error("按名称模糊查询竞赛列表异常：", e);
            return new HashSet<>();
        }
    }

    /**
     * 根据竞赛id获取竞赛详情.
     *
     * @param id 竞赛id
     * @return 竞赛实体
     */
    @Override
    public Competition getCompetitionDescById(Integer id) {
        try {
            Competition competition = competitionDao.selectCompetitionById(id);
            Integer hit = competitionHitDao.getHitByCompetitionId(id);
            Integer like = competitionLikeDao.getLikeByCompetitionId(id);
            LOGGER.debug("hit = [{}], like = [{}]", hit, like);
            if (hit == null) {
                hit = 0;
            }
            if (like == null) {
                like = 0;
            }
            competition.setHit(hit);
            competition.setLike(like);
            return competition;
        } catch (RuntimeException e) {
            LOGGER.error("根据id获取竞赛详情异常:", e);
            return null;
        }
    }

    /**
     * 根据精确竞赛名称获取竞赛详情.
     *
     * @param name 竞赛名称
     * @return 竞赛
     */
    @Override
    public Competition getCompetitionByExactName(String name) {
        try {
            return competitionDao.selectCompetitionByExactName(name);
        } catch (RuntimeException e) {
            LOGGER.error("根据精确竞赛名称获取竞赛详情异常：", e);
            return null;
        }
    }

    /**
     * 更改竞赛.
     *
     * @param competition 竞赛
     * @return 成功返回1，否则-1
     */
    @Override
    public Integer updateCompetition(Competition competition) {
        try {
            return competitionDao.updateCompetition(competition);
        } catch (RuntimeException e) {
            LOGGER.error("更新竞赛异常，竞赛id为[{}]", competition.getId(), e);
            return -1;
        }
    }


    /**
     * 根据页码,每页显示条数,我的竞赛中获得竞赛.
     *
     * @param page   页码
     * @param rows   每页显示的条数
     * @param userId 用户Id
     * @return 竞赛列表
     */
    @Override
    public List<Competition> getCompetitionsByUserId(Integer page,
                                                     Integer rows,
                                                     Integer userId) {
        PageHelper.startPage(page, rows);
        PageHelper.orderBy(QueryOrderEnum
                .USER_COMPETITION_COMP_SIGN_END_TIME.getOrder());
        try {
            return userCompetitionDao
                    .getCompetitionsByUserId(userId);
        } catch (RuntimeException e) {
            LOGGER.error("getCompetitionsByUserId:", e);
            return new ArrayList<>();
        }
    }

}
