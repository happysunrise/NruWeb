package com.happysunrise.nru.managers.imp;

import com.github.pagehelper.PageHelper;
import com.happysunrise.nru.daos.INoticeDao;
import com.happysunrise.nru.enumerations.QueryOrderEnum;
import com.happysunrise.nru.managers.INoticeManager;
import com.happysunrise.nru.models.Notice;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liukaixin on 16/9/22.
 */
@Component
public class NoticeManager implements INoticeManager {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(NoticeManager.class);

    @Autowired
    private INoticeDao noticeDao;

    @Override
    public Integer addNotice(Notice notice) {
        return noticeDao.addNotice(notice);
    }

    @Override
    public List<Notice> getCompetitionNotice(Integer page, Integer rows, Integer userId) {
        try {
            PageHelper.startPage(page, rows);
            PageHelper.orderBy(QueryOrderEnum.CREATE_TIME.getOrder());
            return noticeDao.getCompetitionNotice(userId);
        } catch (RuntimeException e) {
            LOGGER.error("获取我的竞赛通知列表异常, 页码为[{}], 条数为[{}], 用户id为[{}]", page, rows, userId, e);
            return new ArrayList<>();
        }
    }

    @Override
    public List<Notice> getSystemNotice(Integer page, Integer rows, Integer userId) {
        try {
            PageHelper.startPage(page, rows);
            PageHelper.orderBy(QueryOrderEnum.CREATE_TIME.getOrder());
            return noticeDao.getSystemNotice();
        } catch (RuntimeException e) {
            LOGGER.error("获取我的竞赛通知列表异常, 页码为[{}], 条数为[{}], 用户id为[{}]", page, rows, userId, e);
            return new ArrayList<>();
        }
    }
}
