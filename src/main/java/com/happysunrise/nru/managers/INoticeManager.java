package com.happysunrise.nru.managers;

import com.happysunrise.nru.models.Notice;

import java.util.List;

/**
 * Created by liukaixin on 16/9/22.
 */
public interface INoticeManager {

    Integer addNotice(Notice notice);

    List<Notice> getCompetitionNotice(Integer page, Integer rows, Integer userId);

    List<Notice> getSystemNotice(Integer page, Integer rows, Integer userId);
}
