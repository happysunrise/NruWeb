package com.happysunrise.nru.managers;

import com.happysunrise.nru.models.*;

import java.util.List;

/**
 * Created by liukaixin on 16/9/4.
 */
public interface IUserManager {

    /**
     * 用户登录.
     *
     * @param username 用户名
     * @return 用户
     */
    User login(String username);

    /**
     * 用户注册.
     *
     * @param user 用户
     * @return 添加成功与否 添加成功返回1
     */
    Integer register(User user);

    /**
     * 获取用户数量.
     *
     * @return 用户数量
     */
    Integer getUserNum();

    /**
     * 添加我的竞赛.
     *
     * @param competition 我的竞赛
     * @param userId      用户Id
     * @return 结果
     */
    Integer addMyCompetition(Competition competition, Integer userId);

    /**
     * 添加我的团队.
     *
     * @param team   我的团队
     * @return 结果
     */
    Integer addMyTeam(UserTeam team);

    /**
     * 获取我的团队.
     *
     * @param page   页码
     * @param rows   条数
     * @param userId 用户Id
     * @return 我的团队列表
     */
    List<UserTeam> getTeamsByUserId(Integer page, Integer rows, Integer userId);

    /**
     * 获取我的收藏.
     *
     * @param page   页码
     * @param rows   条数
     * @param userId 用户Id
     * @return 我的收藏列表
     */
    List<Favorite> getFavoritesByUserId(Integer page, Integer rows, Integer userId);

    /**
     * 根据团队id和用户获取团队成员详情.
     *
     * @param teamId 团队id
     * @param userId 用户id
     * @return 用户
     */
    User getUserByTeamIdAndUserId(Integer teamId, Integer userId);

    /**
     * 根据id获取用户详情.
     *
     * @param userId 用户id
     * @return 用户
     */
    User getUserInfoById(Integer userId);

    /**
     * 添加我的收藏.
     *
     * @param favorite 我的收藏
     * @return .
     */
    Integer favorite(Favorite favorite);

    /**
     * 获取收藏状态.
     * @param starId 收藏id
     * @param userId 用户id
     * @return 收藏
     */
    Favorite getFavoriteByStarIdAndUserId(Integer starId, Integer userId);

    /**
     * 删除我的收藏.
     *
     * @param starId 收藏id
     * @param userId 用户id
     * @return 结果
     */
    Integer deleteFavorite(Integer starId, Integer userId);

    /**
     * 更新用户.
     *
     * @param user 用户
     * @return 结果
     */
    Integer update(User user);

    /**
     * 添加token到token表.
     *
     * @param id userId
     * @param token token
     * @return 结果
     */
    Integer addToken(Integer id, String token);

    /**
     * 根据userId获取用户名字和图片
     *
     * @param id 用户id
     * @return .
     */
    User getUserNameAndPicById(Integer id);

    /**
     * 添加用户推送.
     *
     * @param userPush 用户推送
     * @return 结果
     */
    Integer addUserPush(UserPush userPush);

    /**
     * 更新用户推送.
     *
     * @param userPush 用户推送
     * @return 结果
     */
    Integer updateUserPush(UserPush userPush);
}
