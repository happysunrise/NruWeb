package com.happysunrise.nru.managers;

import com.happysunrise.nru.models.Competition;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * CompetitionManager接口.
 * <p>
 * Created by liukaixin on 16/7/16.
 */
public interface ICompetitionManager {

    /**
     * 添加竞赛.
     *
     * @param competition 竞赛
     * @return 返回插入结果
     * @throws RuntimeException
     */
    Integer addCompetition(Competition competition);

    /**
     * 获得发布的竞赛.
     *
     * @param page 页码
     * @param rows 每页显示的条数
     * @return 竞赛列表
     */
    List<Competition> getReleasedCompetitionsBy(Integer page, Integer rows);


    /**
     * 根据页码,每页显示条数,竞赛类型获得已发布的竞赛.
     *
     * @param page 页码
     * @param rows 每页显示的条数
     * @param type 类型
     * @return 竞赛列表
     */
    List<Competition> getReleasedCompetitionsByType(
            Integer page, Integer rows, String type);

    /**
     * 根据页码，条数，名称模糊查询竞赛列表.
     *
     * @param page 页码
     * @param rows 条数
     * @param name 竞赛名称（模糊）
     * @return 竞赛集合
     */
    Set<Competition> getReleasedCompetitionsByFuzzyName(
            Integer page, Integer rows, String name);

    /**
     * 根据id获取竞赛详情
     *
     * @param id 竞赛id
     * @return 竞赛实体
     */
    Competition getCompetitionDescById(Integer id);

    /**
     * 根据精确竞赛名称获取竞赛详情.
     *
     * @param name 竞赛名称
     * @return 竞赛
     */
    Competition getCompetitionByExactName(String name);

    /**
     * 更改竞赛.
     *
     * @param competition 竞赛
     * @return 成功返回1，否则-1
     */
    Integer updateCompetition(Competition competition);

    /**
     * 根据页码,每页显示条数,显示我的竞赛.
     *
     * @param page 页码
     * @param rows 每页显示的条数
     * @param userId 用户Id
     * @return 我的竞赛列表
     */
    List<Competition> getCompetitionsByUserId(
            Integer page, Integer rows, Integer userId);

}
