package com.happysunrise.nru.services;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.happysunrise.nru.models.Competition;
import com.happysunrise.nru.models.Result;

/**
 * CompetitionService接口.
 * <p>
 * Created by liukaixin on 16/7/16.
 */
public interface ICompetitionService {
    /**
     * 添加竞赛.
     *
     * @param competition 竞赛
     * @return 添加结果
     */
    Result<Integer> addCompetition(Competition competition);

    /**
     * 只根据页码和条数获得竞赛.
     *
     * @param page 页数
     * @param rows 条数
     * @return 添加结果
     */
    Result<List<Competition>> getReleasedCompetitionsBy(Integer page, Integer rows);

    /**
     * 根据页码和条数以及类型获得竞赛.
     *
     * @param page 页码
     * @param rows 每页显示条数
     * @param type 类型
     * @return 添加结果
     */
    Result<List<Competition>> getReleasedCompetitionByType(
            Integer page, Integer rows, String type);

    /**
     * 根据名称模糊查找竞赛列表.
     *
     * @param page 页码
     * @param rows 每页条数
     * @param name 竞赛名称（模糊）
     * @return 竞赛
     */
    Result<Set<Competition>> getReleasedCompetitionsByFuzzyName(
            Integer page, Integer rows, String name);

    /**
     * 根据竞赛id获取竞赛详情.
     *
     * @param id 竞赛id
     * @return 竞赛实体
     */
    Result<Competition> getCompetitionDescById(Integer id);

    /**
     * 根据精确竞赛名称获取竞赛详情.
     *
     * @param name 精确竞赛名称
     * @return 竞赛
     */
    Result<Competition> getCompetitionDescByExactName(String name);

    /**
     * 更改竞赛.
     *
     * @param competition 竞赛
     * @return 结果
     */
    Result updateCompetition(Competition competition);

    /**
     * 根据用户Id获取我的竞赛.
     *
     * @param page 页码
     * @param rows 每页显示条数
     * @param userId 用户Id
     * @return 添加结果
     */
    Result<List<Competition>> getCompetitionsByUserId(
            Integer page, Integer rows, Integer userId);
}
