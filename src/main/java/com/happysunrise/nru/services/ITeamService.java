package com.happysunrise.nru.services;

import com.happysunrise.nru.models.Apply;
import com.happysunrise.nru.models.Result;
import com.happysunrise.nru.models.Team;
import com.happysunrise.nru.models.TeamMember;

import java.util.List;

/**
 * Created by liukaixin on 16/9/6.
 */
public interface ITeamService {

    /**
     * 获得发布状态的团队.
     *
     * @param page 页码
     * @param rows 条数
     * @return 结果集
     */
    Result<List<Team>> getReleasedTeams(Integer page, Integer rows);

    /**
     * 根据团队id获取团队详情
     *
     * @param id 团队id
     * @return 团队实体
     */
    Result<Team> getTeamDescById(Integer id);

    /**
     * 添加加入团队申请.
     *
     * @param apply 申请
     * @return 结果集
     */
    Result<String> addJoinTeamApply(Apply apply);

    /**
     * 创建团队.
     *
     * @param team 团队
     * @return .
     */
    Result<String> createTeam(Team team);

    /**
     * 根据竞赛id获取团队列表.
     *
     * @param page          页码
     * @param rows          条数
     * @param competitionId 竞赛id
     * @return 结果集
     */
    Result<List<Team>> getTeamsByCompetitionId(Integer page, Integer rows,
                                               Integer competitionId);

    /**
     * 根据团队id和申请人id判断是否已经申请过.
     *
     * @param teamId 团队id
     * @param userId 申请人id
     * @return
     */
    Result<Integer> getApplyStatus(Integer teamId, Integer userId);

    /**
     * 根据id获取申请.
     *
     * @param id 申请的id
     * @return
     */
    Result<Apply> getApplyById(Integer id);

    /**
     * 根据申请id更改状态.
     *
     * @param applyId 申请id
     * @param status  要更改的状态
     * @return
     */
    Result<String> updateApplyStatus(Integer applyId, Integer status);

    /**
     * 获取申请人同比赛其他团队的申请状态.
     *
     * @param applyUserId     申请人id
     * @param competitionName 比赛名称
     * @return 申请状态
     */
    Result<Integer> getOtherApplyStatus(Integer applyUserId, String competitionName);

    /**
     * 根据id获取我的申请列表.
     *
     * @param page   页码
     * @param rows   条数
     * @param userId 用户id
     * @return
     */
    Result<List<Apply>> getMyAppliesByUserId(Integer page, Integer rows, Integer userId);

    Result<List<TeamMember>> getTeamMembers(Integer teamId);
}
