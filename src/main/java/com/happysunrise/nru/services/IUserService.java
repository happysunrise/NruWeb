package com.happysunrise.nru.services;

import com.happysunrise.nru.models.*;

import java.util.List;

/**
 * Created by liukaixin on 16/9/4.
 */
public interface IUserService {

    /**
     * 用户登录, 登录成功返回用户详细信息.
     *
     * @param user 只带用户名和密码的user
     * @return 带有详情的user
     */
    Result<User> login(User user);

    /**
     * 用户注册.
     *
     * @param user user
     * @return String第几位用户
     */
    Result<User> register(User user);

    /**
     * 修改用户信息.
     *
     * @param user 修改的用户信息
     * @return 新用户
     */
    Result<String> update(User user);

    /**
     * 添加我的竞赛.
     *
     * @param competition 竞赛
     * @param userId      用户Id
     * @return 我的竞赛结果
     */
    Result<String> addMyCompetition(Competition competition,
                                    Integer userId);

    /**
     * 添加我的团队.
     *
     * @param team   我的团队
     * @return 我的团队结果
     */
    Result<String> addMyTeam(UserTeam team);

    /**
     * 获取我的团队.
     *
     * @param page   页码
     * @param rows   条数
     * @param userId 用户Id
     * @return 团队结果
     */
    Result<List<UserTeam>> getTeamsByUserId(Integer page, Integer rows, Integer userId);

    /**
     * 获取我的收藏.
     *
     * @param page   页码
     * @param rows   条数
     * @param userId 用户Id
     * @return 收藏结果
     */
    Result<List<Favorite>> getFavoritesByUserId(Integer page, Integer rows, Integer userId);

    /**
     * 根据团队id和用户获取团队成员详情.
     *
     * @param teamId 团队id
     * @param userId 用户id
     * @return
     */
    Result<User> getUserByTeamIdAndUserId(Integer teamId, Integer userId);

    /**
     * 根据用户id获取用户详情.
     *
     * @param userId 用户id
     * @return
     */
    Result<User> getUserInfoById(Integer userId);

    /**
     * 添加我的收藏.
     *
     * @param favorite 我的收藏
     * @return .
     */
    Result<String> favorite(Favorite favorite);

    /**
     * 获取收藏状态.
     *
     * @param starId 收藏id
     * @param userId 用户id
     * @return
     */
    Result<Boolean> getFavoriteOrNot(Integer starId, Integer userId);

    Result<String> cancelFavorite(Integer starId, Integer userId);

    /**
     * 添加用户id和推送用的clientId
     * @param userPush 推送
     * @return
     */
    Result<Integer> addUserPush(UserPush userPush);
}
