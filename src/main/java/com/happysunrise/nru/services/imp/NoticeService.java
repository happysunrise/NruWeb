package com.happysunrise.nru.services.imp;

import com.happysunrise.nru.enumerations.NoticeTypeEnum;
import com.happysunrise.nru.enumerations.ResultTypeEnum;
import com.happysunrise.nru.managers.INoticeManager;
import com.happysunrise.nru.managers.ITeamManager;
import com.happysunrise.nru.models.Notice;
import com.happysunrise.nru.models.Result;
import com.happysunrise.nru.services.INoticeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by liukaixin on 16/9/22.
 */
@Service
public class NoticeService implements INoticeService {

    private static final Logger LOGGER = LoggerFactory
            .getLogger(CompetitionService.class);

    @Autowired
    private INoticeManager noticeManager;
    @Autowired
    private ITeamManager teamManager;

    @Override
    public Result<List<Notice>> getNoticeByUserIdAndCategory(Integer page, Integer rows,
                                                             Integer userId, Integer category) {
        Result<List<Notice>> result = new Result<>();
        result.setCode(ResultTypeEnum.SUCCESS.getValue());
        result.setMessage("获取" + ResultTypeEnum.SUCCESS.getDescription());
        if (category.equals(NoticeTypeEnum.TEAM_NOTICE.getTypeCode())) {
            result.setBean(teamManager.getMyChecksByUserId(page, rows, userId));
        } else if (category.equals(NoticeTypeEnum.COMPOETITION_NOTICE.getTypeCode())) {
            result.setBean(noticeManager.getCompetitionNotice(page, rows, userId));
        } else {
            result.setBean(noticeManager.getSystemNotice(page, rows, userId));
        }
        return result;
    }
}
