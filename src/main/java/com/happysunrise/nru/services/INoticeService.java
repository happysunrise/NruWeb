package com.happysunrise.nru.services;

import com.happysunrise.nru.models.Notice;
import com.happysunrise.nru.models.Result;

import java.util.List;

/**
 * Created by liukaixin on 16/9/22.
 */
public interface INoticeService {

    /**
     * 根据用户id和类别id获取通知.
     *
     * @param page   页数
     * @param rows   条数
     * @param userId 用户id
     * @return
     */
    Result<List<Notice>> getNoticeByUserIdAndCategory(Integer page, Integer rows,
                                                      Integer userId, Integer categoryId);
}
