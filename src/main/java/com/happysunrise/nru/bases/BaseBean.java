package com.happysunrise.nru.bases;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by liukaixin on 16/7/21.
 */
public class BaseBean implements Serializable {

    private static final long serialVersionUID = 1880270714923798776L;
    /**
     * id
     */
    protected Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
