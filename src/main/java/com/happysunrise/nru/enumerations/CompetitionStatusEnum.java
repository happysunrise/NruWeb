package com.happysunrise.nru.enumerations;

/**
 * Created by puhui on 2016/8/12.
 */
public enum CompetitionStatusEnum {

    FORBID(0, "禁止"),
    AUDIT(1, "审核中"),
    RELEASE(2, "发布"),
    ARCHIEVE(3, "归档");

    private final Integer status;
    private final String description;

    CompetitionStatusEnum(Integer status, String description) {
        this.status = status;
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }
}
