package com.happysunrise.nru.enumerations;

/**
 * Created by liukaixin on 16/9/22.
 */
public enum ApplyStatusEnum {

    NOT_CHECK(1, "未审核"),
    AGREE(2, "已同意"),
    REJECT(3, "已拒绝");

    private final Integer status;
    private final String description;

    ApplyStatusEnum(Integer status, String description) {
        this.status = status;
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

}
