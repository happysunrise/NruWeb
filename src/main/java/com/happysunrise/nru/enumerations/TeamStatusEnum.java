package com.happysunrise.nru.enumerations;

/**
 * Created by liukaixin on 16/9/6.
 */
public enum TeamStatusEnum {

    FORBID(0, "禁止"),
    RELEASE(1, "发布"),
    ARCHIEVE(2, "归档");

    private final Integer status;
    private final String description;

    TeamStatusEnum(Integer status, String description) {
        this.status = status;
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }
}
