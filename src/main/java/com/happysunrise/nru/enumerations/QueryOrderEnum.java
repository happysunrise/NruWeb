package com.happysunrise.nru.enumerations;

/**
 * Created by liukaixin on 2016/8/22.
 */
public enum QueryOrderEnum {
    /**
     * 按COMPETITION报名截止日期逆序排列
     */
    COMPETITION_SIGN_END_TIME_DESC("top desc, sign_up_end_time desc"),
    /**
     * 创建日期
     */
    CREATE_TIME("create_time desc"),

    /**
     * 按USER_COMPETITION表中的报名截止时间逆序排列
     */
    USER_COMPETITION_COMP_SIGN_END_TIME("comp_sign_end_time desc");


    private final String order;

    QueryOrderEnum(String order) {
        this.order = order;
    }

    public String getOrder() {
        return order;
    }
}
