package com.happysunrise.nru.enumerations;

/**
 * Created by liukaixin on 16/10/16.
 */
public enum NoticeTypeEnum {
    COMPOETITION_NOTICE(1, "竞赛通知"),
    TEAM_NOTICE(2, "团队通知"),
    SYSTEM_NOTICE(3, "系统通知");

    private final Integer typeCode;
    private final String description;

    NoticeTypeEnum(Integer typeCode, String description) {
        this.typeCode = typeCode;
        this.description = description;
    }

    public Integer getTypeCode() {
        return typeCode;
    }

    public String getDescription() {
        return description;
    }
}
