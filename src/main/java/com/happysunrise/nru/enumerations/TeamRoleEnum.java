package com.happysunrise.nru.enumerations;

/**
 * Created by Jacob on 2016/10/4.
 */
public enum TeamRoleEnum {

    CREATOR(0,"创建者"),
    MEMBER(1,"成员");

    private final Integer value;
    private final String description;

    TeamRoleEnum(Integer value,String description){
        this.value = value;
        this.description = description;
    }

    public Integer getValue() {
        return value;
    }

    public String getDescription() {
        return description;
    }
}
