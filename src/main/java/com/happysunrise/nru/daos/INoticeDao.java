package com.happysunrise.nru.daos;

import com.happysunrise.nru.models.Notice;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by liukaixin on 16/9/22.
 */
@Repository
public interface INoticeDao {

    List<Notice> getNoticesById(@Param("userId") Integer userId);

    List<Notice> getNoticeByUserIdAndCategory(@Param("userId") Integer userId,
                                              @Param("category") Integer category);

    Integer addNotice(@Param("notice") Notice notice);

    List<Notice> getCompetitionNotice(@Param("userId") Integer userId);

    List<Notice> getSystemNotice();
}
