package com.happysunrise.nru.daos;

import com.happysunrise.nru.models.Competition;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Competition表的数据访问方法.
 * <p>
 * Created by liukaixin on 16/7/16.
 */
@Repository
public interface ICompetitionDao {

    /**
     * 添加竞赛.
     *
     * @param competition 竞赛实体
     * @return 插入一条竞赛, 成功返回1
     */
    Integer addCompetition(Competition competition);

    /**
     * 根据状态查询竞赛.
     *
     * @param status 竞赛状态,0:禁止 1:审核 2:发布 3:归档
     * @return 竞赛列表
     */
    List<Competition> selectCompetitionsByStatus(@Param("status") Integer status);

    /**
     * 根据竞赛类型,参赛地区,参赛方式,竞赛状态查询竞赛.
     *
     * @param type   竞赛类型,2位数
     * @param status 竞赛状态
     * @return 竞赛列表
     */
    List<Competition> selectCompetitionsByConditions(
            @Param("type") String type,
            @Param("status") Integer status);

    /**
     * 根据竞赛id查询竞赛详情
     *
     * @param id 竞赛id
     * @return 竞赛详情
     */
    Competition selectCompetitionById(@Param("id") Integer id);

    /**
     * 根据精确竞赛名称查询竞赛详情.
     *
     * @param name 竞赛名称
     * @return 竞赛
     */
    Competition selectCompetitionByExactName(@Param("name") String name);

    /**
     * 根据名称模糊查询竞赛列表
     *
     * @param name   名称（模糊）
     * @param status 状态
     * @return 竞赛列表
     */
    Set<Competition> selectCompetitionsByFuzzyNameAndStatus(@Param("name") String name,
                                                            @Param("status") Integer status);

    /**
     * 更改竞赛.
     *
     * @param competition 竞赛
     * @return 返回更新条数，只有1是正确的
     */
    Integer updateCompetition(Competition competition);

}
