package com.happysunrise.nru.daos;

import com.happysunrise.nru.models.Apply;
import com.happysunrise.nru.models.Notice;
import com.happysunrise.nru.models.Team;
import com.happysunrise.nru.models.TeamMember;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 团队数据访问接口.
 * <p>
 * Created by liukaixin on 16/9/6.
 */
@Repository
public interface ITeamDao {

    /**
     * 根据状态查询团队.
     *
     * @param status 状态
     * @return 列表
     */
    List<Team> getTeamsByStatus(@Param("status") Integer status);

    /**
     * 根据团队id查询团队详情.
     *
     * @param id 团队id
     * @return 团队详情
     */
    Team getTeamDescById(@Param("id") Integer id);

    /**
     * 创建团队.
     *
     * @param team 团队
     */
    void createTeam(Team team);

    /**
     * 根据竞赛id获取团队列表.
     *
     * @param competitionId 竞赛id
     * @return 团队列表
     */
    List<Team> getTeamsByCompetitionId(@Param("competitionId") Integer competitionId);

    /**
     * 根据团队id获取团队名称和竞赛名称.
     *
     * @param id 团队id
     * @return team
     */
    Team getTeamNameAndCompetitionNameById(@Param("id") Integer id);


    /**
     * 根据名称查找teamId.
     *
     * @param name 名称
     * @return teamId
     */
    Integer getTeamIdByName(@Param("name") String name);

    /**
     * 根据创始人id和竞赛id获取团队id.
     *
     * @param userId        创始人id
     * @param competitionId 竞赛id
     * @return teamId
     */
    Integer getTeamIdByUserIdAndCompId(@Param("userId") Integer userId,
                                       @Param("competitionId") Integer competitionId);

    /**
     * 根据userId获取我要审核的团队申请.
     *
     * @param userId 用户id（team_creator_id)
     * @return notice
     */
    List<Apply> getMyChecksByUserId(@Param("userId") Integer userId);

    List<TeamMember> getTeamMembers(@Param("teamId") Integer teamId);

    Integer updateTeamMemberNum(@Param("teamId") Integer teamId);
}
