package com.happysunrise.nru.daos;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by liukaixin on 16/9/21.
 */
@Repository
public interface ICompetitionHitDao {

    Integer getHitByCompetitionId(@Param("competitionId") Integer competitionId);

}
