package com.happysunrise.nru.daos;

import com.happysunrise.nru.models.Apply;
import com.happysunrise.nru.models.User;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

/**
 * Created by liukaixin on 16/9/19.
 */
@Repository
public interface IApplyDao {

    void addJoinTeamApply(Apply apply);

    /**
     * 根据团队id和申请人id获取申请
     *
     * @param teamId 团队id
     * @param userId 申请人id
     * @return
     */
    Apply getApplyByTeamIdAndApplyUserId(@Param("teamId") Integer teamId, @Param("userId") Integer userId);

    /**
     * 根据id获取申请.
     *
     * @param id 申请id
     * @return
     */
    Apply getApplyById(@Param("id") Integer id);

    /**
     * 根据申请id更改状态.
     *
     * @param applyId 申请id
     * @param status 要更改的状态
     * @return
     */
    Integer updateApplyStatus(@Param("applyId") Integer applyId, @Param("status") Integer status);

    /**
     * 获取申请人同比赛其他团队的申请状态.
     *
     * @param applyUserId     申请人id
     * @param competitionName 比赛名称
     * @return 申请状态
     */
    Set<Integer> getOtherApplyStatus(@Param("applyUserId") Integer applyUserId,
                                     @Param("competitionName") String competitionName);

    /**
     * 根据用户id获取我的申请.
     *
     * @param userId 用户id
     * @return
     */
    List<Apply> getMyAppliesByUserId(@Param("userId") Integer userId);

    /**
     * 根据申请人id和团队id获取申请状态.
     *
     * @param teamId 团队id
     * @param userId 申请人id
     * @return
     */
    Integer getApplyStatusByTeamIdAndApplyUserId(@Param("teamId") Integer teamId,
                                                 @Param("userId") Integer userId);

    /**
     * 根据团队创建者id获取要审核的团队申请.
     *
     * @param userId 团队创建者id
     * @return
     */
    List<Apply> getMyChecksByUserId(@Param("userId") Integer userId);
}
