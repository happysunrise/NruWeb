package com.happysunrise.nru.daos;

import com.happysunrise.nru.models.User;
import com.happysunrise.nru.models.UserPush;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 用户数据访问层.
 *
 * Created by liukaixin on 16/9/4.
 */
@Repository
public interface IUserDao {

    /**
     * 添加用户.
     *
     * @param user 用户
     * @return 1 添加成功
     */
    Integer addUser(User user);

    /**
     * 根据名称查找用户.
     *
     * @param username 用户名
     * @return 用户实体
     */
    User getUserByName(@Param("username") String username);

    /**
     * 获取用户数量.
     *
     * @return 用户数量
     */
    Integer getUserCount();

    /**
     * 根据用户id获取用户详情.
     *
     * @param userId 用户id
     * @return 用户实体
     */
    User getUserById(@Param("userId") Integer userId);

    /**
     *
     * @param user 用户
     * @return 结果
     */
    Integer update(@Param("user") User user);

    /**
     * 向token表添加一条token记录.
     *
     * @param id userId
     * @param token token
     * @return 结果
     */
    Integer addToken(@Param("userId") Integer id, @Param("token") String token);

    /**
     * 修改token.
     *
     * @param id userId
     * @param token token
     * @return 结果
     */
    Integer updateToken(@Param("userId") Integer id, @Param("token") String token);

    /**
     * 根据用户id获取用户名和用户图片
     *
     * @param id    id
     * @return .
     */
    User getUserNameAndPicById(@Param("id") Integer id);

    /**
     * 添加用户推送.
     *
     * @param userPush 用户推送
     * @return 结果
     */
    Integer addUserPush(@Param("userPush") UserPush userPush);

    /**
     * 添加用户推送.
     *
     * @param userPush 用户推送
     * @return 结果
     */
    Integer updateUserPush(@Param("userPush") UserPush userPush);
}
