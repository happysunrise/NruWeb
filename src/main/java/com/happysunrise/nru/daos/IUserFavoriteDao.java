package com.happysunrise.nru.daos;

import com.happysunrise.nru.models.Favorite;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Jacob on 2016/9/17.
 */

public interface IUserFavoriteDao {



    /**
     * 根据用户Id来获取我的收藏.
     *
     * @param userId 用户Id
     * @return
     */
    List<Favorite> getFavoritesByUserId(@Param("userId") Integer userId);

    /**
     * 添加我的收藏.
     *
     * @param favorite 我的收藏
     * @return .
     */
    Integer addFavorite(@Param("favorite") Favorite favorite);

    /**
     * 获取收藏.
     *
     * @param starId 收藏id
     * @param userId 用户id
     * @return
     */
    Favorite getFavoritesByStarIdAndUserId(@Param("starId") Integer starId,
                                           @Param("userId") Integer userId);

    Integer deleteFavorite(@Param("startId") Integer starId,
                        @Param("userId") Integer userId);
}
