package com.happysunrise.nru.daos;

import com.happysunrise.nru.models.TeamMember;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by Jacob on 2016/10/5.
 */
@Repository
public interface ITeamMemberDao {

    Integer addTeamMember(@Param("teamMember")TeamMember teamMember);
}
