package com.happysunrise.nru.daos;

import com.happysunrise.nru.models.Competition;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Jacob on 2016/9/15.
 */
@Repository
public interface IUserCompetitionDao {

    /**
     * 根据用户Id来获取竞赛
     *
     * @param userId 用户Id
     * @return 竞赛列表
     */
    List<Competition> getCompetitionsByUserId(
            @Param("userId") Integer userId);

    /**
     * 添加我的竞赛
     *
     * @param competition 竞赛实体
     * @param userId      用户Id
     * @return 插入我的竞赛，成功返回1
     */
    Integer addMyCompetition(@Param("competition") Competition competition,
                             @Param("userId") Integer userId);
}
