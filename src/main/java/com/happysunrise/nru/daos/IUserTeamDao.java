package com.happysunrise.nru.daos;

import com.happysunrise.nru.models.Favorite;
import com.happysunrise.nru.models.Team;
import com.happysunrise.nru.models.UserTeam;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * Created by Jacob on 2016/9/16.
 */
public interface IUserTeamDao {


    /**
     * 添加我的团队.
     *
     * @param team      我的团队
     * @return  插入我的团队，成功返回1
     */
    Integer addMyTeam(@Param("team") UserTeam team);

    /**
     * 根据用户Id来获取我的团队.
     *
     * @param userId    用户Id
     * @return 团队列表
     */
    List<UserTeam> getTeamsByUserId(@Param("userId") Integer userId);

}
