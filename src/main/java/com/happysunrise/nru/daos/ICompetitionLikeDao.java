package com.happysunrise.nru.daos;

import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * Created by liukaixin on 16/9/21.
 */
@Repository
public interface ICompetitionLikeDao {

    Integer getLikeByCompetitionId(@Param("competitionId") Integer competitionId);

}
