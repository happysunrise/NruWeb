package com.happysunrise.nru.services;

import com.happysunrise.nru.MyApplication;
import com.happysunrise.nru.models.Competition;
import com.happysunrise.nru.models.Result;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Created by liukaixin on 16/7/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyApplication.class)
@WebAppConfiguration
public class CompetitionServiceTest {

    @Autowired
    ICompetitionService competitionService;

    /**
     * 测试添加竞赛.
     */
    @Test
    @Transactional
    public void
    should_return_success_when_insert_a_new_competition_successfully() {
        Competition competition = getCompetition();
        Result<Integer> result = competitionService.addCompetition(competition);
        assertEquals("竞赛发布成功,等待审核", result.getMessage());
    }

    /**
     * 测试获得发布的竞赛.
     */
    @Test
    @Transactional
    public void
    should_return_success_when_query_competition_without_conditions() {
        for (int i = 0; i < 20; i++) {
            Competition competition = getCompetition();
            competition.setName(String.valueOf(i));
            competitionService.addCompetition(competition);
        }
        Result<List<Competition>> result = competitionService.getReleasedCompetitionsBy(1, 10);
        assertEquals(Result.SUCCESS, result.getCode());
    }

    @Test
    @Transactional
    public void
    should_return_success_when_query_competition_by_conditions() {
//        for (int i = 0; i < 20; i++) {
//            Competition competition = getCompetition();
//            competition.setName(String.valueOf(i));
//            competitionService.addCompetition(competition);
//        }
//        Map<String, Object> conditions = new HashMap<>();
//        conditions.put("type", "1");
//        conditions.put("area", "0");
//        conditions.put("joinWay", 0);
//        Result<Set<Competition>> result = competitionService.getReleasedCompetitionByCondition(
//                1, 10, conditions);
//        assertEquals(10, result.getBean().size());
    }

    private Competition getCompetition() {
        Competition competition = new Competition();
//        competition.setName("xxx competition");
//        competition.setArea("0");
//        competition.setAward("23");
//        competition.setCreateTime(new Date());
//        competition.setHit(21);
//        competition.setHost("243");
//        competition.setHostUserId(22);
//        competition.setIntro("");
//        competition.setItem("");
//        competition.setPic("");
//        competition.setPredictAchieveTime(new Date());
//        competition.setJoinWay(0);
//        competition.setRequire("");
//        competition.setWeb("");
//        competition.setSchedule("");
//        competition.setSignUpEndTime(new Date());
//        competition.setTheme("");
//        competition.setStatus(2);
//        competition.setTypeIds("1,2,3,4");
//        competition.setUpdateTime(new Date());
        return competition;
    }

}
