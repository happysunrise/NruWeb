package com.happysunrise.nru.managers;

import com.happysunrise.nru.MyApplication;
import com.happysunrise.nru.models.Competition;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;

/**
 * Created by liukaixin on 16/8/12.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyApplication.class)
@WebAppConfiguration
public class CompetitionManagerTest {

    @Autowired
    private ICompetitionManager competitionManager;

    @Before
    public void setUp() {
        for (int i = 0; i < 20; i++) {
            Competition competition = getCompetition();
            competition.setName(String.valueOf(i));
            competitionManager.addCompetition(competition);
        }
    }

    /**
     * 测试pageHelper的使用,当数据有20条,取第1页的10条数据时,应获得大小为10的集合.
     */
    @Test
    @Transactional
    public void should_get_10_when_page_1_rows_10() {
        List<Competition> competitionSet1 = competitionManager.getReleasedCompetitionsBy(1, 10);
        assertEquals(10, competitionSet1.size());
    }

    /**
     * 测试pageHelper的使用,当数据有20条,取第2页的10条数据时,集合大小应为10并不同于第1页第10条数据.
     */
    @Test
    @Transactional
    public void should_get_10_and_different_from_page_1_when_page_2_rows_10() {
        List<Competition> competitionSet1 = competitionManager.getReleasedCompetitionsBy(1, 10);
        List<Competition> competitionSet2 = competitionManager.getReleasedCompetitionsBy(2, 10);

        assertEquals(10, competitionSet2.size());
        for (Competition competition : competitionSet1) {
            assertFalse(competitionSet2.contains(competition));
        }
    }

    /**
     * 测试pageHelper的使用,当数据有20条,取第3页的10条数据时,应返回集合大小为0.
     */
    @Test
    @Transactional
    public void should_get_0_when_page_3_rows_10() {
        List<Competition> competitionSet3 = competitionManager.getReleasedCompetitionsBy(3, 10);
        assertEquals(0, competitionSet3.size());
    }

    private Competition getCompetition() {
        Competition competition = new Competition();
//        competition.setName("xxx competition");
//        competition.setArea("1,2,3");
//        competition.setAward("23");
//        competition.setCreateTime(new Date());
//        competition.setHit(21);
//        competition.setHost("243");
//        competition.setHostUserId(22);
//        competition.setIntro("");
//        competition.setItem("");
//        competition.setPic("");
//        competition.setPredictAchieveTime(new Date());
//        competition.setJoinWay(0);
//        competition.setRequire("");
//        competition.setWeb("");
//        competition.setSchedule("");
//        competition.setSignUpEndTime(new Date());
//        competition.setTheme("");
//        competition.setStatus(2);
//        competition.setTypeIds("");
//        competition.setUpdateTime(new Date());
        return competition;
    }
}
