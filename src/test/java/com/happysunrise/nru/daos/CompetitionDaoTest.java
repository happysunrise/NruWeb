package com.happysunrise.nru.daos;

import com.happysunrise.nru.MyApplication;
import com.happysunrise.nru.models.Competition;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Created by liukaixin on 2016/8/12.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MyApplication.class)
@WebAppConfiguration
public class CompetitionDaoTest {

    @Autowired
    private ICompetitionDao competitionDao;

    /**
     * 测试根据状态查询数据.
     */
    @Test
    @Transactional
    public void should_return_right_size_of_set_when_query_data_from_competition() {
        Integer originNum = competitionDao.selectCompetitionsByStatus(2).size();
        Competition competition = getCompetition();
        competitionDao.addCompetition(competition);
        assertEquals(1, competitionDao.selectCompetitionsByStatus(2).size() - originNum);
    }

    /**
     * 测试根据id查询竞赛详情.
     */
    @Test
    @Transactional
    public void
    should_get_competition_description_right_when_query_it_by_id() {
        assertNotNull(competitionDao.selectCompetitionById(729));
    }

    @Test
    @Transactional
    public void
    should_get_competition_description_right_by_exact_name() {
        Competition competition = getCompetition();
        competitionDao.addCompetition(competition);
        assertEquals(competition.getName(),
                competitionDao.selectCompetitionByExactName(competition.getName()).getName());
    }

    private Competition getCompetition() {
        Competition competition = new Competition();
//        competition.setName("xxx competition");
//        competition.setArea("1,2,3");
//        competition.setAward("23");
//        competition.setCreateTime(new Date());
//        competition.setHit(21);
//        competition.setHost("243");
//        competition.setHostUserId(22);
//        competition.setIntro("");
//        competition.setItem("");
//        competition.setPic("");
//        competition.setPredictAchieveTime(new Date());
//        competition.setJoinWay(0);
//        competition.setRequire("");
//        competition.setWeb("");
//        competition.setSchedule("");
//        competition.setSignUpEndTime(new Date());
//        competition.setTheme("");
//        competition.setStatus(2);
//        competition.setTypeIds("");
//        competition.setUpdateTime(new Date());
        return competition;
    }

}
