package com.happysunrise.nru;

import io.restassured.RestAssured;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;


import static io.restassured.RestAssured.given;

import static io.restassured.RestAssured.get;

import static org.hamcrest.Matchers.equalTo;
/**
 * Competition集成测试.
 *
 * Created by liukaixin on 2016/8/19.
 */

public class CompetitionTest {

    private String competition = "{" +
            "  \"areaIds\": \"string\"," +
            "  \"award\": \"string\"," +
            "  \"createTime\": \"2016-08-20T08:47:21.334Z\"," +
            "  \"hit\": 0," +
            "  \"host\": \"string\"," +
            "  \"hostUserId\": 0," +
            "  \"id\": 0," +
            "  \"intro\": \"string\"," +
            "  \"item\": \"string\"," +
            "  \"joinWay\": 0," +
            "  \"name\": \"string\"," +
            "  \"pic\": \"string\"," +
            "  \"predictArchieveTime\": \"2016-08-20T08:47:21.334Z\"," +
            "  \"require\": \"string\"," +
            "  \"schedule\": \"string\"," +
            "  \"signUpEndTime\": \"2016-08-20T08:47:21.334Z\"," +
            "  \"status\": 0," +
            "  \"theme\": \"string\"," +
            "  \"typeIds\": \"string\"," +
            "  \"updateTime\": \"2016-08-20T08:47:21.334Z\"," +
            "  \"web\": \"string\"" +
            "}";

    private String conditions = "{" +
            "  \"type\": \"1\"," +
            "  \"area\": \"0\"," +
            "  \"joinWay\": 0" +
            "}";

    @Before
    public void setUp() throws Exception {
        //指定 URL 和端口号
        RestAssured.baseURI =
                "http://localhost:8080/racing-union/mobile/competition";
        RestAssured.port = 80;
    }

    @Test
    public void
    should_return_ok_and_add_successfully_when_request_add() {
        given().contentType("application/json;charset=UTF-8")
                .request().body(competition).expect()
                .statusCode(HttpStatus.OK.value()).body("code", equalTo(1))
                .body("message", equalTo("竞赛发布成功,等待审核"))
                .when()
                .post("/add");
    }

    @Test
    public void
    should_return_ok_and_correct_body_when_request_get_competitions() {
        given().pathParam("page", 1).pathParam("rows", 10)
                .when().get("/getReleasedCompetitions/{page}/{rows}")
                .then()
                .statusCode(HttpStatus.OK.value());

    }

    @Test
    public void
    should_return_ok_http_status_and_correct_body_when_request_get_by_conditions() {
        given().pathParam("page", 1).pathParam("rows", 10)
                .contentType("application/json;charset=UTF-8")
                .request().body(conditions)
                .when().post("/getReleasedCompetitionsByConditions/{page}/{rows}")
                .then().statusCode(HttpStatus.OK.value());
    }

    @Test
    public void
    should_return_ok_http_status_when_query_by_id() {
        given().pathParam("id", 729)
                .when().get("/getCompetitionDescById/{id}")
                .then().statusCode(HttpStatus.OK.value());
    }
}
