#竞赛联盟

###Swagger
- url: [http://localhost:8080/swagger-ui.html]()

###业务接口
#### 竞赛  
- 添加竞赛  

    - 地址: [http:localhost:8080/racing-union/competition/add]()
    - 请求方式: POST
    - 请求体: Competition
- 查询竞赛  
     - 发布状态的竞赛 
        
         - 地址: [http:localhost:8080/racing-unioncompetition/getReleasedCompetitions/{page}/{rows}]()
         - 请求方式: GET
         - 请求参数: page(页码) rows(请求条数)
         
     - 根据类型查询发布状态的竞赛 
        
         - 地址: [http:localhost:8080/racing-union/competition/getReleasedCompetitionsByType/{page}/{rows}/{type}]()
         - 请求方式: GET
         - 请求参数: page(页码) rows(请求条数) type(竞赛类型)

     - 根据名称精确查询某个竞赛

         - 地址: [http:localhost:8080/racing-union/competition/getCompetitionDescByExactName/{name}]()
         - 请求方式: GET
         - 请求参数: name(名称)

     - 根据id查询竞赛

         - 地址: [http:localhost:8080/racing-union/competition/getCompetitionDescById/{id}]()
         - 请求方式: GET
         - 请求参数: id

     - 根据名称模糊查询竞赛

         - 地址: [http:localhost:8080/racing-union/competition/getReleasedCompetitionsByFuzzyName/{page}/{rows}/{name}]()
         - 请求方式: GET
         - 请求参数: id

- 更新竞赛
    - 地址：[http:localhost:8080/racing-union/competition/updateCompetition]()
    - 请求方式：POST
    - 请求参数：competition的json格式